#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;
#[macro_use] extern crate serde_derive;
extern crate pnet;

use std::io;

use rocket_contrib::serve::StaticFiles;
use rocket::response::NamedFile;
use rocket_contrib::json::JsonValue;

use pnet::datalink;

#[derive(Serialize)]
struct TableRow1 (bool, String, String);
#[derive(Serialize)]
struct TableRow2 (bool, String, String, String);
#[derive(Serialize)]
struct TableRow3 (bool, String, String, String, String);


#[get("/")]
pub fn index() -> io::Result<NamedFile> {
    NamedFile::open("static/index.html")
}

#[get("/page/interfaces", format = "json")]
fn page_interfaces() -> JsonValue {
    json!({
        "title": "Interfaces",
        "t": [
            {
                "l": "All interfaces",
                "p": "interfaces"
            }
        ],
        "c": [
            {
                "_t": "datatable",
                "c": [
                    ["" ,"32px"], ["" ,"32px"], ["Interface", "100"], ["Hw. addr", "100"]
                ],
                "s": "interfaces"
            }
        ]
    })
}

#[get("/data/interfaces", format = "json")]
fn data_interfaces() -> JsonValue {
    let mut rows = Vec::new();
    for iface in datalink::interfaces() {
        if !iface.is_loopback() {
            let mut flags = String::with_capacity(4);
            if iface.is_up() {
                flags.push('R')
            }
            let row = TableRow3(true, "interfaces/".to_string(), flags, (&iface.name).to_string(), iface.mac_address().to_string());
            rows.push(row);
        }
    }
    json!({
        "rows": rows,
    })
}

fn main() {
    rocket::ignite()
        .mount("/", routes![index, page_interfaces, data_interfaces])
        .mount("/static", StaticFiles::from("static"))
        .launch();
}