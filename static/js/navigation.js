var currentPage;

document.addEventListener("DOMContentLoaded", function () {
    var nav = document.getElementById('nav');
    nav.addEventListener('click', function (event) {
        event.preventDefault();
        var rootLi = event.target.parentNode;

        // Handle group header
        if (rootLi.classList.contains("group")) {
            var subLi;

            for (var i = 0; i < rootLi.childNodes.length; i++) {
                if (rootLi.childNodes[i].tagName == "UL") {
                    subLi = rootLi.childNodes[i];
                }
            }

            if (subLi.style.display == "block") {
                subLi.style.display = "none";
            } else {
                subLi.style.display = "block";
            }
        } else {
            var target = event.target.dataset.target;
            if (target != undefined) {
                switchPage(target);
            }
        }

    });

    var eventRoot = document.getElementById('eventroot');
    eventRoot.addEventListener('click', function (event) {
        for (var i = 0; i < event.path.length; i++) {
            if (event.path[i].dataset != undefined) {
                if (event.path[i].dataset.hasOwnProperty('target')) {
                    var link = event.path[i];
                    if (link.dataset.tab == "yes") {
                        var tabs = document.querySelectorAll("#tabs a");
                        for (var j = 0; j < tabs.length; j++) {
                            tabs[j].classList.remove('active');
                        }
                        link.classList.add('active');
                    }
                    if(link.dataset.action == "1"){
                        action(event.path[i].dataset.target);
                    }else{
                        switchPage(event.path[i].dataset.target);
                    }
                }
            }
        }
    })
});

function switchPage(target, isTab) {
    "use strict";
    currentPage = target;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/page/' + target);
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.onreadystatechange = function () {
        var DONE = 4; // readyState 4 means the request is done.
        var OK = 200; // status 200 is a successful return.
        if (xhr.readyState === DONE) {
            if (xhr.status === OK) {

                history.pushState({}, null, "/#" + target.replace("/", "."));

                render(JSON.parse(xhr.responseText));
                console.log(xhr.responseText);
            }
        }
    };

    var buttons = document.querySelectorAll('#nav a');
    for (var i = 0; i < buttons.length; i++) {
        if (buttons[i].dataset.target == target) {
            buttons[i].classList.add('active');
        } else {
            buttons[i].classList.remove('active');
        }
    }

    xhr.send(null);
}

function action(target){
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/action/' + target);
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.onreadystatechange = function () {
        var DONE = 4; // readyState 4 means the request is done.
        var OK = 200; // status 200 is a successful return.
        if (xhr.readyState === DONE) {
            if (xhr.status === OK) {
                var result = JSON.parse(xhr.responseText);
                if(result.hasOwnProperty('redirect')){
                    switchPage(result.redirect);
                }
            }
        }
    };
    xhr.send(null);
}

window.onpopstate = function (event) {
    "use strict";
    if (window.location.hash) {
        var hash = window.location.hash.substring(1);
        var target = hash.replace(".", "/");
        switchPage(target);
    }
};

if (window.location.hash) {
    var hash = window.location.hash.substring(1);
    var target = hash.replace(".", "/");
    switchPage(target);
}
