var tabsContainer = document.getElementById('tabs');
var contentContainer = document.getElementById('content');

function render(page) {
    "use strict";

    tabsContainer.innerHTML = '';
    contentContainer.innerHTML = '';
    document.title = page.title + " - TrafficTux 1.x";
    document.getElementById('title').innerText = page.title;

    // Render tabs
    for (var i = 0; i < page.t.length; i++) {
        var li = document.createElement('LI');
        var a = document.createElement('A');
        a.innerText = page.t[i].l;
        a.dataset.target = page.t[i].p;
        a.dataset.tab = "yes";

        if (window.location.hash) {
            var hash = window.location.hash.substring(1);
            if (hash.replace(".", "/") == page.t[i].p) {
                a.classList.add('active');
            }
        }

        li.appendChild(a);
        tabsContainer.appendChild(li);
    }

    // Render components for this page
    for (i = 0; i < page.c.length; i++) {
        renderComponent(page.c[i]);
    }
}

function renderComponent(component) {
    "use strict";
    var type = component._t;

    switch (type) {
        case "datatable":
            renderDataTable(component);
            break;
        case "header":
            renderHeader(component);
            break;
        case "value":
            renderValue(component);
            break;
        case "toolbar":
            renderToolbar(component);
            break;
        case "textbox":
            renderTextbox(component);
            break;
        case "checkbox":
            renderCheckbox(component);
            break;
        case "videoplayer":
            renderVideoPlayer(component);
            break;
        case "combobox":
            renderCombobox(component);
            break;
    }
}

function renderVideoPlayer(component) {
    "use strict";
    var player = document.createElement("IMG");
    player.src = component.u;
    player.classList.add('videoplayer');
    contentContainer.appendChild(player);
}

function renderCheckbox(component) {
    "use strict";
    var input = document.createElement("INPUT");
    input.type = "checkbox";
    input.name = component.k;
    input.checked = component.c;

    renderRow(component.n, input, component.k, true)
}

function renderTextbox(component) {
    "use strict";
    var input = document.createElement("INPUT");
    input.name = component.k;
    input.value = component.v;

    renderRow(component.n, input, component.k, true)
}

function renderCombobox(component) {
    "use strict";
    var input = document.createElement("SELECT");
    for (var i = 0; i < component.o.length; i++) {
        var option = document.createElement('OPTION');
        option.value = component.o[i].v;
        option.innerText = component.o[i].l;
        input.appendChild(option);
    }
    input.name = component.k;
    input.value = component.v;

    renderRow(component.n, input, component.k, true)
}

function renderToolbar(component) {
    "use strict";
    var toolbar = document.createElement("DIV");
    toolbar.classList.add("toolbar");

    for (var i = 0; i < component.b.length; i++) {
        var element = component.b[i];
        switch (element._t) {
            case "b":
                var link = document.createElement("A");
                link.innerText = "Back";
                link.onclick = function (event) {
                    history.back();
                };
                toolbar.appendChild(link);
                break;
            case "a":
                var link = document.createElement("A");
                link.innerText = "Ok";
                link.onclick = function (event) {
                    history.back();
                };
                toolbar.appendChild(link);
                var link2 = document.createElement("A");
                link2.innerText = "Apply";
                link2.onclick = function (event) {
                    history.back();
                };
                toolbar.appendChild(link2);

                break;
            case "sv":
                var link = document.createElement("A");
                link.innerText = "Save";
                toolbar.appendChild(link);
                var link2 = document.createElement("A");
                link2.innerText = "Verify";
                toolbar.appendChild(link2);

                break;
            case "s":
                var spacer = document.createElement("SPAN");
                toolbar.appendChild(spacer);
                break;
            case "n":
                var link = document.createElement("A");
                link.innerText = element.l;
                link.dataset.target = element.t;
                if(element.a){
                    link.dataset.action = "1";
                }
                toolbar.appendChild(link);
                break;
        }
    }
    contentContainer.appendChild(toolbar);
}

function renderHeader(component) {
    "use strict";

    var header = document.createElement("H2");
    header.innerText = component.t;
    contentContainer.appendChild(header);
}

function renderValue(component) {
    "use strict";
    var span = document.createElement('SPAN');
    span.innerText = component.v;
    renderRow(component.n, span, component.n);
}

function renderRow(name, value, id, input) {
    "use strict";
    var row = document.createElement("DIV");
    row.classList.add("row");
    row.dataset.id = id;

    var n = document.createElement("DIV");
    n.innerText = name;
    n.classList.add('name');

    var v = document.createElement("DIV");
    v.classList.add('value');
    if (input) {
        v.classList.add('input');
    }
    v.appendChild(value);

    row.appendChild(n);
    row.appendChild(v);
    contentContainer.appendChild(row);

}

function renderDataTable(component) {
    "use strict";
    var table = document.createElement("TABLE");
    var thead = document.createElement("THEAD");
    var theadTr = document.createElement("TR");
    table.classList.add('table');
    table.cellSpacing = 0;
    table.appendChild(thead);
    thead.appendChild(theadTr);

    var containerRect = contentContainer.getBoundingClientRect();
    var containerWidth = containerRect.width - 60;
    var relativeWidth = 0;
    for (var i = 0; i < component.c.length; i++) {
        if (component.c[i][1].indexOf("px") !== -1) {
            containerWidth -= parseInt(component.c[i][1].replace("px", ""), 10);
        } else {
            relativeWidth += parseInt(component.c[i][1], 10);
        }
    }

    for (i = 0; i < component.c.length; i++) {
        var th = document.createElement("TH");
        th.innerText = component.c[i][0];

        if (component.c[i][1].indexOf("px") !== -1) {
            th.style.width = component.c[i][1];
        } else {
            var width = parseInt(component.c[i][1], 10);
            th.style.width = ((width / relativeWidth) * containerWidth) + "px";
        }

        theadTr.appendChild(th);
    }
    var tbody = document.createElement("TBODY");
    tbody.id = "datatable-" + component.s;
    table.appendChild(tbody);
    contentContainer.appendChild(table);

    var dataUrl = "/data/" + component.s;
    loadTableData(tbody, dataUrl);
}

function loadTableData(tbody, url) {
    "use strict";

    var xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.onreadystatechange = function () {
        var DONE = 4; // readyState 4 means the request is done.
        var OK = 200; // status 200 is a successful return.
        if (xhr.readyState === DONE) {
            if (xhr.status === OK) {

                var json = JSON.parse(xhr.responseText);
                var data = json.rows;

                for (var i = 0; i < data.length; i++) {
                    var tr = document.createElement('TR');
                    if (!data[i][0]) {
                        tr.classList.add('disabled');
                    }
                    tr.dataset.target = data[i][1];
                    var buttons = document.createElement('TD');
                    tr.appendChild(buttons);
                    for (var j = 2; j < data[i].length; j++) {
                        var td = document.createElement('TD');
                        td.innerText = data[i][j];
                        tr.appendChild(td);
                    }
                    tbody.appendChild(tr);
                }

                if (json.hasOwnProperty('next')) {
                    loadTableData(tbody, json.next);
                }

            }
        }
    };

    xhr.send(null);

}

