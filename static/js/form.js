document.addEventListener("DOMContentLoaded", function () {
    document.getElementById('eventroot').addEventListener('change', onFormChange);
    document.getElementById('eventroot').addEventListener('keydown', onFormChange);
    document.getElementById('eventroot').addEventListener('click', onToolbar);
});

function onFormChange(event) {
    "use strict";
    if (event.target.tagName == "INPUT" || event.target.tagName == "SELECT") {
        var element = event.target;

        if (!element.dataset.hasOwnProperty('old')) {
            element.dataset.old = element.value;
        }
        element.parentNode.parentNode.classList.add('changed');
    }
}

function onToolbar(event) {
    "use strict";
    if (event.target.tagName == "A" && event.target.parentNode.classList.contains('toolbar')) {
        event.preventDefault();
        var action = event.target.innerText;
        switch (action) {
            case "Verify":
                verifyForm();
                break;
            case "Save":
                saveForm();
                break;
        }
    }
}

function gatherFormData() {
    "use strict";
    var data = {};
    var elements = document.querySelectorAll('input');
    for (var i = 0; i < elements.length; i++) {
        var element = elements[i];
        if (element.type == "checkbox") {
            data[element.name] = element.checked;
        } else {
            data[element.name] = element.value;
        }
    }
    return data;
}

function encodeQuery(object) {
    "use strict";
    var encodedString = '';
    for (var prop in object) {
        if (object.hasOwnProperty(prop)) {
            if (encodedString.length > 0) {
                encodedString += '&';
            }
            encodedString += encodeURI(prop + '=' + object[prop]);
        }
    }
    return encodedString;
}

function verifyForm() {
    "use strict";
    var data = gatherFormData();

    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/verify/' + currentPage);
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function () {
        var DONE = 4; // readyState 4 means the request is done.
        var OK = 200; // status 200 is a successful return.
        if (xhr.readyState === DONE) {
            if (xhr.status === OK) {
                var json = JSON.parse(xhr.responseText);
                console.log(json);
                if (json.hasOwnProperty('values')) {
                    updateValues(json.values);
                }
            }
        }
    };

    xhr.send(encodeQuery(data));
}

function saveForm() {
    "use strict";
    var data = gatherFormData();

    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/save/' + currentPage);
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function () {
        var DONE = 4; // readyState 4 means the request is done.
        var OK = 200; // status 200 is a successful return.
        if (xhr.readyState === DONE) {
            if (xhr.status === OK) {
                var json = JSON.parse(xhr.responseText);
                console.log(json);
                if (json.hasOwnProperty('values')) {
                    updateValues(json.values);
                }
                if (json.hasOwnProperty('redirect')) {
                    switchPage(json.redirect);
                }
            }
        }
    };

    xhr.send(encodeQuery(data));

}

function updateValues(dataset) {
    "use strict";
    for (var key in dataset) {
        if (dataset.hasOwnProperty(key)) {
            var value = dataset[key];
            var row = document.querySelector('div[data-id="' + key + '"] .value span');
            row.innerText = value;
        }
    }
}
